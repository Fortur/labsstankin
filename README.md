# Лабораторная работа 1

![none](https://gitlab.com/Fortur/labsstankin/raw/master/lab1/model.png)

# Диаграмма классов:
[Код](https://gitlab.com/Fortur/labsstankin/blob/master/lab1/f2)  
![none](https://gitlab.com/Fortur/labsstankin/raw/master/lab1/s2.PNG)

# Диаграмма прецедентов:

[Код тут](https://gitlab.com/Fortur/labsstankin/blob/master/lab1/f3)  
![none](https://gitlab.com/Fortur/labsstankin/raw/master/lab1/s3.PNG)

# Лабораторная работа 2

Модель IDEF0  
![none](https://gitlab.com/Fortur/labsstankin/raw/master/lab2/l21.png)  
PDC  
![none](https://gitlab.com/Fortur/labsstankin/raw/master/lab2/l22.png)  
DFD Chek  
![none](https://gitlab.com/Fortur/labsstankin/raw/master/lab2/l23.png)  

# Лабораторная работа 4
**Определение требований к модели**

* Формальное определение объекта моделирования (процесса) – Разработка API.
* Формальное определение точки зрения (владелец, руководитель) – Руководитель проекта.
* Формальное определение цели моделирования (вопросы к модели) – Визуализировать процесс разработки/проектирования IPI.
* Формальное определение темы курсового проекта (наименование информационной системы) – Разработка серверного интерфейса для производственного сетевого приложения.

* IDEF0-диаграммы: *  
![none](https://gitlab.com/Fortur/labsstankin/raw/master/lab4/l21.png) 
![none](https://gitlab.com/Fortur/labsstankin/raw/master/lab4/l22.png) 
![none](https://gitlab.com/Fortur/labsstankin/raw/master/lab4/l23.png) 
![none](https://gitlab.com/Fortur/labsstankin/raw/master/lab4/l24.png) 
![none](https://gitlab.com/Fortur/labsstankin/raw/master/lab4/l25.png)  
[Код тут.rsf](https://gitlab.com/Fortur/labsstankin/blob/master/lab4/lab4.rsf)  

# Лабораторная работа 5
### Определение основных средств автоматизации
* Определение конфигурации технических средств (рабочие станции, серверы, другое оборудование)
  * Рабочие станции (ПК)
  * Сервер
* Определение конфигурации программных средств (одноуровневые, многоуровневые, встроенные, распределенные)
  * Многоуровневые
* Определение допустимых видов хранилищ и их размещения
  * База данных на сервере  
 
### Разработка диаграмм в RAMUS
![none](https://gitlab.com/Fortur/labsstankin/raw/master/lab5/l51.png) 
![none](https://gitlab.com/Fortur/labsstankin/raw/master/lab5/l52.png)  
